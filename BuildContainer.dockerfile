FROM ubuntu:focal
RUN apt-get -y update
RUN apt-get install curl bash -y
RUN curl -Lv https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O 
RUN dpkg -i packages-microsoft-prod.deb
RUN apt-get -y update
RUN apt-get install -y dotnet-sdk-3.1 dotnet-sdk-6.0
RUN mkdir -p $HOME/nethat
COPY /* $HOME/nethat
WORKDIR $HOME/nethat
RUN dotnet tool install Cake.Tool --version=2.0.0



